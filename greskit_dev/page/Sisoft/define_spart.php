<?php
//=====================================//
//== Define Page Spare Part  		 ==//
//=====================================//

DEFINE("PATH_JVMOVEMENT", "index.php?sjvmove=true&page=sjvmove");
DEFINE("PATH_EXJVMOVEMENT", "index.php?exjvmove=true&page=exjvmove");
DEFINE('SYNC','&sync=ok');

//=====================================//
//== Define Title Spare Part  		 ==//
//=====================================//
DEFINE("TJVMOVEMENT", "JOURNAL MOVEMENT");

//=====================================//
//== Define Query Spare Part  		 ==//
//=====================================//
DEFINE("UPSTAMV", "SELECT state FROM invent_journal_movement");
DEFINE("JVMOVEMENT", "SELECT J.report_id REPORT_ID, J.jvmovement_id ID, J.date_jvmovement Date, item_description Item_Name, B.brand_short_name Brand, '' Cost_Center, W.warehouse_name Warehouse, S.state_journal_movement State, take_by Take_By, number_of_stock Number_of_Stock, J.WorkOrderNo Work_Order, J.remark1 Remarks, A.AssetNo AssetNo, P.PlantCode Plant_Code, P.PlantId PlantId, J.section_id Section, J.item_id Item_id, WT.WorkTrade WorkTrade FROM invent_journal_movement J, invent_item I, invent_brand B, invent_warehouse W, invent_state_journal_movement S, work_order O, asset A, plant P, work_trade WT WHERE J.item_id=I.item_id AND I.brand_id=B.brand_id AND I.warehouse_id=W.warehouse_id AND J.state=S.state_journal_movement_id AND J.WorkOrderNo=O.WorkOrderNo AND O.AssetID=A.AssetID AND A.PlantID=P.PlantId AND J.section_id=WT.WorkTradeID");

DEFINE("COMBWORDER", "SELECT WorkOrderNo, WorkOrderNo FROM work_order WHERE Hidden='no' ORDER BY WorkOrderNo DESC");
DEFINE("COMBWHREQS","SELECT report_id,report_id FROM invent_journal_movement GROUP BY report_id DESC");
DEFINE('COMBPLANT','SELECT PlantId, PlantCode FROM plant WHERE PlantID NOT IN ("PL000017", "PL000016", "PL000014","PL000015","PL000018","PL000019","PL000020") ORDER BY PlantCode');
DEFINE('COMWOTRADE','SELECT WorkTradeID, WorkTrade FROM work_trade');
