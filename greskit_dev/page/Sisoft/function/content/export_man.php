<?php
	function export_man(){ 
		$position = 'SELECT PS.PositionID, PS.PositionCode FROM employee EP, position PS WHERE EP.Positions=PS.PositionID AND (DepartmentID="DP000029" OR DepartmentID="DP000035") GROUP BY PositionID ORDER BY PositionCode ASC';
	
		$content .= '<br/><div class="ade">EXPORT MAN POWER TO EXCEL</div>';
		$content .= '<div class="toptext" align="center">'._USER_VIEW_.'</div>';
		$content .= '<br/><div class="form-style-2"><form action="'.PATH_EXMAN.ADD.POST.'" method="post" enctype="multipart/form-data">
							<fieldset><div class="card-header text-center">Man Power</div>
								<div class="row">
									<div class="col-6">
										<table>
											<tr>
												<td width="120"><span class="name"> Range Date </td><td>:</td><td>'.date_je(array('date_rec_1',$_REQUEST['date_rec_1'])).' </td>
											</tr>
											<tr>
												<td width="20"><span class="name"></td><td></td><td>'.date_je(array('date_rec_2',$_REQUEST['date_rec_2'])).'</td>
											</tr>
											<tr>
												<td width="20"><span class="name"><br/></td><td></td><td></td>
											</tr>
											<tr><td></td><td></td><td><input class="form-submit" type="submit" value="Submit"></td></tr>
										</table>
									</div>
									<div class="col-6">
										<table>
											<tr>
												<td width="120"><span class="name">Position </td><td>:</td><td>'.combo_je(array($position,'position','position',180,'<option value="">-</option>',$_REQUEST['position'])).'</td>
											</tr>
											<tr>
												<td width="120"><span class="name">WO Type </td><td>:</td><td>'.combo_je(array(COMWOTYPE,'wotype','wotype',180,'<option value="">-</option>',$_REQUEST['wotype'])).'</td>
											</tr>
											<tr>
												<td width="120"><span class="name">Plant </td><td>:</td><td>'.combo_je(array(COMBPLANT,'plant','plant',180,'<option value="">-</option>',$_REQUEST['plant'])).'</td>
											</tr>
										</table>
									</div>
								</div>
							</fieldset>
							</form></div>';
		
		//------ Aksi ketika post data -----//
		if(isset($_REQUEST['post'])){
			$data_table = '';
			$recdate1 = convert_date_time(array($_REQUEST['date_rec_1'],1));
			$recdate2 = convert_date_time(array($_REQUEST['date_rec_2'],1));
		
			$sql = 'SELECT EM.FirstName, PS.PositionName, PL.PlantCode, WT.WorkTypeDesc, WO.WorkOrderNo, WO.ProblemDesc, DATE_FORMAT(WM.start_date,"%m/%d/%Y %h:%i") start_date, DATE_FORMAT(WM.finish_date,"%m/%d/%Y %h:%i") finish_date, time_to_sec(timediff(WM.finish_date, WM.start_date))/3600 duration FROM work_order WO, work_order_manpower WM, employee EM, position PS, asset AE, plant PL, work_type WT WHERE WO.WorkOrderNo=WM.WorkOrderNo AND WM.EmployeeID=EM.EmployeeID AND EM.Positions=PS.PositionID AND WO.AssetID=AE.AssetID AND AE.PlantID=PL.PlantId AND WO.WorkTypeID=WT.WorkTypeID AND WM.start_date BETWEEN "'.$recdate1.'" AND "'.$recdate2.'" AND WM.finish_date BETWEEN "'.$recdate1.'" AND "'.$recdate2.'" AND PS.PositionID LIKE "%'.$_REQUEST['position'].'%" AND PL.PlantId LIKE "%'.$_REQUEST['plant'].'%" AND WO.WorkTypeID LIKE "%'.$_REQUEST['wotype'].'%"'; 
			$result = mysql_query($sql) or die ('FAILED TO GENERATE QUERY'); 
			while($result_now= mysql_fetch_array($result)){
				$data_table .= '
							<tr>	
								<td>'.$result_now[0].'</td>
								<td>'.$result_now[1].'</td>
								<td>'.$result_now[2].'</td>
								<td>'.$result_now[3].'</td>
								<td>'.$result_now[4].'</td>
								<td>'.$result_now[5].'</td>
								<td>'.$result_now[6].'</td>
								<td>'.$result_now[7].'</td>
								<td>'.$result_now[8].'</td>
							</tr>
				';
			}
			
			gen_man_excel(array($sql,'format1',0,'mp_report'));
			$report = '<div align="center"><a href="'._ROOT_.'mp_report.xlsx" class="btn btn-info" role="button">Download Excel</a></div>';
			
			
			$content .= $report.'    
				  <div class="content-wrapper">
					<div class="row">
					  <div class="col-lg-12 grid-margin stretch-card">
						<div class="card">
						  <div class="card-body">
							<table id="asset-data" class="table table-bordered" style="width:100%">
							  <thead>
								<tr>
									<th>Employee Name</th>
									<th>Position</th>
									<th>Plant</th>
									<th>Work Type</th>
									<th>WO No</th>
									<th>Problem</th>
									<th>Start Date</th>
									<th>Finish Date</th>
									<th>Duration</th>
								</tr>
							  </thead>
							  <tbody>
								'.$data_table.'
							  </tbody>
							</table>
						  </div>
						</div>
					  </div>
					</div>
				  </div>
				  <!-- content-wrapper ends -->
			';
		}
		
		$content .= wo_js(); 
		return $content;
	}
	
	function gen_man_excel($data){
		$sql = $data[0];
		$page = $data[1];
		$sheet = $data[2];
		$name = $data[3];
		$content = ''; 
		$result = mysql_query($sql) or die ('FAILED TO EXPORT EXCEL'); 
		error_reporting(E_ALL);
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("TPC INDO PLASTIC AND CHEMICALS")
							 ->setLastModifiedBy("TPC INDO PLASTIC AND CHEMICALS")
							 ->setTitle("Office 2007 XLSX Document")
							 ->setSubject("Office 2007 XLSX Document")
							 ->setDescription("document for Office 2007 XLSX, generated using PHP.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("TPC INDO PLASTIC AND CHEMICALS");
		
		if(strcmp($page,'format1')==0){
			$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Employee Name');
			$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Position');
			$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Plant');
			$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Work Type');
			$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Work Order No');
			$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Problem Description');
			$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Start Date');
			$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Finish Date');
			$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Duration');
			
			$i=2;
			while($result_now= mysql_fetch_array($result)){
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $result_now[0]);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $result_now[1]);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $result_now[2]);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $result_now[3]);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $result_now[4]);
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $result_now[5]);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $result_now[6]);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $result_now[7]);
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $result_now[8]);
				$i++;
			}
		}
		
		$objPHPExcel->getActiveSheet()->setTitle('Man Power Report');	
		$objPHPExcel->setActiveSheetIndex($sheet);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save(str_replace('', '.xlsx', _ROOT_.$name.'.xlsx'));
		
		return $content;
	}
	
	function man_js(){
		$content="
			<script>
				$('#asset-data').DataTable({
					dom: 'Bfrtip',
					scrollX: 200,					
					buttons: [
						{
							className: 'green glyphicon glyphicon-file',
							extend: 'pdfHtml5',
							messageTop: 'Asset Data',
							orientation: 'landscape',
							download: 'open',
							pageSize: 'LEGAL'
						},
						{
							extend: 'csv',
							text: 'CSV',
							exportOptions: {
								modifier: {
									search: 'none'
								}
							}
						},
						{
							extend: 'excelHtml5',
							text: 'Excel',
							exportOptions: {
								modifier: {
									page: 'current'
								}
							}
						},
						{
							extend: 'print',
							text: 'Print',
							autoPrint: false
						}
					]
				});
			</script>
		";
		
		return $content;
	}
?>