<?php
	function export_movement(){ 
    	$content .= '<br/><div class="ade">EXPORT WAREHOUSE REQUISITION TO EXCEL</div>';
		$content .= '<div class="toptext" align="center">'._USER_VIEW_.'</div>';
    	$content .= '<br/><div class="form-style-2"><form action="'.PATH_EXJVMOVEMENT.ADD.POST.'" method="post" enctype="multipart/form-data">
							<fieldset><div class="card-header text-center">Warehouse Requisition</div>
								<div class="row">
									<div class="col-6">
										<table>
											<tr>
												<td width="120"><span class="name"> Requested Date </td><td>:</td><td>'.date_je(array('date_rec_1',$_REQUEST['date_rec_1'])).' </td>
											</tr>
											<tr>
												<td width="20"><span class="name"> </td><td></td><td>'.date_je(array('date_rec_2',$_REQUEST['date_rec_2'])).'</td>
											</tr>
											<tr><td></td><td></td><td><input class="form-submit" type="submit" value="Submit"></td></tr>
										</table>
									</div>
									<div class="col-6">
										<table>
											<tr>
												<td width="120"><span class="name">Plant. </td><td>:</td><td>'.combo_je(array('SELECT PlantId, PlantCode FROM plant WHERE PlantID NOT IN ("PL000017", "PL000016", "PL000014","PL000015","PL000018","PL000019","PL000020") ORDER BY PlantCode','plant','plant',180,'<option value="">-</option>',$_REQUEST['plant'])).'</td>
											</tr>
											<tr>
												<td width="120"><span class="name">Section. </td><td>:</td><td>'.combo_je(array(COMWOTRADE,'section','section',180,'<option value="">-</option>',$_REQUEST['section'])).'</td>
											</tr>
										</table>
									</div>
								</div>
							</fieldset>
							</form></div>';
						// return $content;
							
    	//------ Aksi ketika post data -----//
		if(isset($_REQUEST['post'])){
			$data_table = '';
			$recdate1 = convert_date(array($_REQUEST['date_rec_1'],2));
			$recdate2 = convert_date(array($_REQUEST['date_rec_2'],2));
			$sql = JVMOVEMENT. ' AND A.PlantID LIKE "%'.$_REQUEST['plant'].'%" AND J.section_id LIKE "%'.$_REQUEST['section'].'%" AND DATE(J.date_jvmovement) BETWEEN "'.$recdate1.'" AND "'.$recdate2.'"'; 
			
			$result = mysql_query($sql) or die ('FAILED TO GENERATE QUERY'); 
			while($result_now= mysql_fetch_array($result)){
				//print_r($result_now);
				$date = date("d/m/Y", strtotime($result_now[2]));
				$data_table .= '
							<tr>	
								<td>'.$date.'</td>
								<td>'.$result_now[0].'</td>
								<td>'.$result_now[10].'</td>
								<td>'.$result_now[13].'</td>
								<td>'.$result_now[12].'</td>
								<td>'.$result_now[11].'</td>
								
								
								
							</tr>
				';
			}
			
			//=========GENERATER EXCEL=============================
			gen_movement_excel(array($sql,'format1',0,'movement_report'));
			$report = '<div align="center"><a href="'._ROOT_.'movement_report.xlsx" class="btn btn-info" role="button">Download Excel</a></div>';
			
			$content .= $report.'    
				  <div class="content-wrapper">
					<div class="row">
					  <div class="col-lg-12 grid-margin stretch-card">
						<div class="card">
						  <div class="card-body">
							<table id="movement-data" class="table table-bordered" style="width:100%">
							  <thead>
								<tr>
									
									<th> Date </th>
									<th> WR NO </th>
									<th>WO NO</th>
									<th>Plant</th>
									<th>Asset</th>
									<th>Remarks</th>
									
									
									
								</tr>
							  </thead>
							  <tbody>
								'.$data_table.'
							  </tbody>
							</table>
						  </div>
						</div>
					  </div>
					</div>
				  </div>
				  <!-- content-wrapper ends -->
			';
			
			//gen_asset_excel(array($sql,'format1',0,'asset_report'));
			//$report = '<div align="center"><a href="'._ROOT_.'asset_report.xlsx" class="btn btn-info" role="button">Download Excel</a></div>';
		}
		
		//$content .= $report;
		$content .= movement_js(); 
		return $content;
	}
	
	
	function gen_movement_excel($data){
		$sql = $data[0];
		$page = $data[1];
		$sheet = $data[2];
		$name = $data[3];
		$content = ''; 
		$result = mysql_query($sql) or die ('FAILED TO EXPORT EXCEL'); 
		error_reporting(E_ALL);
		$objPHPExcel = new PHPExcel();
		$objReader = PHPExcel_IOFactory::createReader('CSV');
        $objReader->setInputEncoding('ISO-8859-1');
		$objPHPExcel->getProperties()->setCreator("GRESKIT")
							 ->setLastModifiedBy("GRESKIT")
							 ->setTitle("Office 2007 XLSX Document")
							 ->setSubject("Office 2007 XLSX Document")
							 ->setDescription("document for Office 2007 XLSX, generated using PHP.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("GRESKIT");
		
		if(strcmp($page,'format1')==0){
			
			$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Date');
			$objPHPExcel->getActiveSheet()->setCellValue('B1', 'WR NO');
			$objPHPExcel->getActiveSheet()->setCellValue('C1', 'WO NO');
			$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Plant');
			$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Asset');
			
			$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Item Code');
			$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Item Desc (NAV)');
			$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Qty');
			$objPHPExcel->getActiveSheet()->setCellValue('I1', 'UOM');
			$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Section');
			$objPHPExcel->getActiveSheet()->setCellValue('K1', 'Remarks');
			
			// $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Critically');
			// $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Auth. Employee');
			// $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Supplier Name');
			// $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Manufacture');
			// $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Model Number');
			// $objPHPExcel->getActiveSheet()->setCellValue('M1', 'Serial Number');
			// $objPHPExcel->getActiveSheet()->setCellValue('N1', 'Warranty');
			// $objPHPExcel->getActiveSheet()->setCellValue('O1', 'Warranty Notes');
			// $objPHPExcel->getActiveSheet()->setCellValue('P1', 'Warranty Date');
			// $objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Asset Note');
			// $objPHPExcel->getActiveSheet()->setCellValue('R1', 'Acquired Date');
			// $objPHPExcel->getActiveSheet()->setCellValue('S1', 'Sold Date');
			// $objPHPExcel->getActiveSheet()->setCellValue('T1', 'Area');
			// $objPHPExcel->getActiveSheet()->setCellValue('U1', 'Plant');
			
			$i=2;
			$j=1;
			//mengambil tabel invent_journal_movement
			while($result_now= mysql_fetch_array($result)){
				$query_data_isi = 'SELECT quantity,remark,item_id,nama_barang_gres,nama_barang_nav from invent_trans_journal_movement WHERE report_id="' . $result_now[0] . '"';
				$result_data_isi =mysql_query($query_data_isi);
				//mengambil tabel invent_trans_journal_movement
				// $result_now_quantity= mysql_fetch_array($result_data_isi);
				while($result_now_quantity= mysql_fetch_array($result_data_isi)){
					//print_r($result_now_quantity);
				$unit_item = 'SELECT unit FROM invent_unit A, invent_item B WHERE A.id_unit=B.id_unit AND B.item_id="' . $result_now_quantity[2] . '"';
				$r_uitem =  mysql_query($unit_item);
				$rs_uitem =  mysql_fetch_array($r_uitem);
				$unit = $rs_uitem[0];
				$date = date("d/m/Y", strtotime($result_now[2]));
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $date);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $result_now[0]);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $result_now[10]);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $result_now[13]);
				
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $result_now[12]);
				// 
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $result_now_quantity[2]);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, utf8_encode($result_now_quantity[4]));
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $result_now_quantity[0]);
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $unit);
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $result_now[17]);
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $result_now[11]);
				//$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $result_now[7]);
				// $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $result_now[8]);
				// $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $result_now[9]);
				// $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $result_now[10]);
				// $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $result_now[11]);
				// $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $result_now[12]);
				// $objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $result_now[13]);
				// $objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $result_now[14]);
				// $objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $result_now[15]);
				// $objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $result_now[16]);
				// $objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $result_now[17]);
				// $objPHPExcel->getActiveSheet()->setCellValue('S'.$i, $result_now[18]);
				// $objPHPExcel->getActiveSheet()->setCellValue('T'.$i, $result_now[21]);
				// $objPHPExcel->getActiveSheet()->setCellValue('U'.$i, $result_now[22]);
				$i++; $j++;
				}
				
			}
		}
		// ob_end_clean();
		// header('Content-Type: application/vnd.ms-excel');
		$objPHPExcel->getActiveSheet()->setTitle('WR Report');	
		$objPHPExcel->setActiveSheetIndex($sheet);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		
		$objWriter->save(str_replace('', '.xlsx', _ROOT_.$name.'.xlsx'));
		
		return $content;
	}
	
	function movement_js(){
		$content="
			<script>
				$('#movement-data').DataTable({
					dom: 'Bfrtip',
					scrollX: 200,					
					buttons: [
						{
							className: 'green glyphicon glyphicon-file',
							extend: 'pdfHtml5',
							messageTop: 'Asset Data',
							orientation: 'landscape',
							download: 'open',
							pageSize: 'LEGAL'
						},
						{
							extend: 'csv',
							text: 'CSV',
							exportOptions: {
								modifier: {
									search: 'none'
								}
							}
						},
						{
							extend: 'excelHtml5',
							text: 'Excel',
							exportOptions: {
								modifier: {
									page: 'current'
								}
							}
						},
						{
							extend: 'print',
							text: 'Print',
							autoPrint: false
						}
					]
				});
			</script>
		";
		return $content;
	}
     
	
?>