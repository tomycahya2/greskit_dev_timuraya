<?php
	header('Origin:xxx.com');
    header('Access-Control-Allow-Origin:*');
    include('conf.php');
	
	$con = new mysqli(host,user,pass,dbase);
	if($con -> connect_errno){
        printf("Connection error: %s\n", $con->connect_error);
    }
	
	$num=array();
	
	$query_wo = 'SELECT COUNT(*) WO FROM work_order WHERE WorkTypeID<>"WT000002"';
	$result_wo = mysqli_query($con,$query_wo);
	$data = mysqli_fetch_assoc($result_wo);
	$num['wo'] = $data['WO'];
	
	$query_pm = 'SELECT COUNT(*) PM FROM work_order WHERE WorkTypeID="WT000002"';
	$result_pm = mysqli_query($con,$query_pm);
	$data = mysqli_fetch_assoc($result_pm);
	$num['pm'] = $data['PM'];
	
	$query_ck = 'SELECT COUNT(DISTINCT(id_checklist_history)) CK FROM checklist_history WHERE date=CURDATE() ';
	$result_ck = mysqli_query($con,$query_ck);
	$data = mysqli_fetch_assoc($result_ck);
	$num['ck'] = $data['CK'];

	echo json_encode($num);
?>