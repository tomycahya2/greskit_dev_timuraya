<?php
	header('Origin:xxx.com');
    header('Access-Control-Allow-Origin:*');
    include('conf.php');

    $con = new mysqli(host,user,pass,dbase);
	if($con -> connect_errno){
        printf("Connection error: %s\n", $con->connect_error);
    }
	
	if(ISSET($_POST['sign']))
		$sign=$_POST['sign'];
	else	
		$sign='';
	
    $temp='';
    if ($sign=='onlyone') {
    	$id_checklist=$_POST['id_checklist'];
    	$id_master=$_POST['id_master'];
    	$id_form=$_POST['id_form'];
    	$content=$_POST['content'];

    	$query = 'UPDATE checklist_history SET description="'.$content.'" WHERE id_master_checklist="'.$id_master.'" AND id_checklist_history="'.$id_checklist.'" AND id_form_checklist="'.$id_form.'"';
    	$result = mysqli_query($con,$query);
    	if($result){
			$temp='successed';
		}
		else{
			$temp='failed';
		}
		echo $temp;
    }
    else if($sign=='insertwo'){
		$assetid=$_POST['assetid'];
		$content=$_POST['content'];

	   $result = mysqli_query($con,'SELECT COUNT(*) row FROM work_order');
	   $resultnow=mysqli_fetch_assoc($result); 
	   $numrow=$resultnow['row']+1; $woid=get_new_code('WO',$numrow);

	   $query = 'INSERT INTO work_order (WorkOrderNo,AssignID,AssetID,CreatedID,DepartmentID,FailureCauseID,RequestorID,WorkPriorityID,WorkStatusID,WorkTradeID,WorkTypeID,ProblemDesc) VALUES("'.$woid.'","EP000001","'.$assetid.'","EP000001","DP000001","FL000001","EP000001","WP000001","WS000001","WR000001","WT000011","'.$content.'")'; 
       $result = mysqli_query($con,$query);
       if($result) echo $woid;
       else echo 'Failed';    	
    }
    else{
		$id_checklist = $_POST['id_checklist']; 
		$id_value = $_POST['id_value']; 
		$id_history = $_POST['id_history'];  
		$type = $_POST['type'];
		
		$id_cl = json_decode($id_checklist); 
		$id_val = json_decode($id_value); 
		
		$i=0; $content = ''; $error = '';
		while($i<sizeof($id_cl)){
			if($type=='history'){
				$query = 'UPDATE checklist_history SET description="'.$id_val[$i].'" WHERE id_master_checklist="'.$id_cl[$i].'" AND id_checklist_history="'.$id_history.'"';
			}else if($type=='asset'){
				$check = explode('_',$id_cl[$i]);
				$query = 'UPDATE checklist_history SET description="'.$id_val[$i].'" WHERE id_master_checklist="'.$check[1].'" AND id_checklist_history="'.$check[0].'"';
			}
			$result = mysqli_query($con,$query);
			if(!$result){
				$error = 'Checklist '.$id_val[$i].' failed to update <br/>';
			}
			$i++;
		}
		
		if(empty($error)){
			$content = 'Update data successed';
		}else{
			$content = $error;
		}
		
		echo $content;
	}
    //echo $id_checklist.'<br/>'.$id_value.'<br/>'.$id_history;
?>